import Vue from "vue";
import App from "./App.vue";
import MethaUI from "./entry"

Vue.config.productionTip = false;

Vue.use(MethaUI)

new Vue({
  render: h => h(App)
}).$mount("#app");
