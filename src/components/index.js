import FormInput from './Form/FormInput/index.vue'
import FormInputMask from './Form/FormInputMask/index.vue'
import FormSelect from './Form/FormSelect/index.vue'
import FormCheckbox from './Form/FormCheckbox/index.vue'
import FormUpload from './Form/FormUpload/index.vue'
import FormFooter from './Form/FormFooter/index.vue'

// Buttons
import ButtonDispatch from './Button/ButtonDispatch/index.vue'

export const componentsPlugin = {
  FormInput,
  FormInputMask,
  FormSelect,
  FormFooter,
  FormCheckbox,
  FormUpload,
  ButtonDispatch
}