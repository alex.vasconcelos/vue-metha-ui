import { mount, createLocalVue } from '@vue/test-utils'
import { merge } from "lodash"

import FormInputMask from './index.vue'
import VueTheMask from 'vue-the-mask'

const localVue = createLocalVue()
localVue.use(VueTheMask)

const props = {
  label: 'Field',
  type: 'text',
  value: '12345678900',
  mask: ["(00) 00000-0000"],
  readonly: false
}

function createWrapper(overrides) {
  const defaultMountingOptions = {
    localVue
  }

  return mount(FormInputMask, merge(defaultMountingOptions, overrides))
}

describe('Form-Input-Mask', () => {

  it("renders correctly", () => {
    const propsData = props
    const wp = createWrapper({ propsData })
    expect(wp.html()).toMatchSnapshot()
  })

})