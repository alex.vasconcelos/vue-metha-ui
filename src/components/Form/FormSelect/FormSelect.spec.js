import { mount } from '@vue/test-utils'
import { merge } from "lodash"

import FormSelect from './index.vue'

const props = {
  label: 'Field',
  type: 'text',
  value: 'Field value A',
  options: [
    'Field value A',
    'Field value B',
    'Field value C'
  ],
  readonly: false
}

function createWrapper(overrides) {
  const defaultMountingOptions = {
  }

  return mount(FormSelect, merge(defaultMountingOptions, overrides))
}

describe('Form-Select', () => {

  it("renders correctly", () => {
    const propsData = props
    const wp = createWrapper({ propsData })
    expect(wp.html()).toMatchSnapshot()
  })

})