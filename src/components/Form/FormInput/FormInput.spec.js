import { mount } from '@vue/test-utils'
import { merge } from "lodash"

import FormInput from './index.vue'

const props = {
  label: 'Field',
  type: 'text',
  value: 'Field value',
  readonly: false
}

function createWrapper(overrides) {
  const defaultMountingOptions = {
  }

  return mount(FormInput, merge(defaultMountingOptions, overrides))
}

describe('Form-Input', () => {

  it("renders correctly", () => {
    const propsData = props
    const wp = createWrapper({ propsData })
    expect(wp.html()).toMatchSnapshot()
  })

})