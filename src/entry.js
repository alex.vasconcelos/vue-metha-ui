import { componentsPlugin } from './components'

import VueTheMask from 'vue-the-mask'
// import SvgIcon from 'vue-svgicon'

//import './assets/scss/components/_vue-svgicon.scss'

function install(Vue) {
  if (install.installed) return;
  install.installed = true;

  Vue.use(VueTheMask)
  // Vue.use(SvgIcon)

  for (const component in componentsPlugin) {
    const name = componentsPlugin[component].name;
    if (name) Vue.component(name, componentsPlugin[component]);
    else console.warn(`Warning! Component ${component} with name undefined!`)
  }
}

const plugin = {
  install
};

let GlobalVue = null;
if (typeof window !== "undefined") {
  GlobalVue = window.Vue;
} else if (typeof global !== "undefined") {
  GlobalVue = global.vue;
}

if (GlobalVue) {
  GlobalVue.use(plugin);
}

export default install;