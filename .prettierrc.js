module.exports = {
  trailingComma: "es5",
  tabSize: 2,
  semi: true,
  singleQuote: true,
  printWidth: 150,
  bracketSpacing: true,
  jsxBracketSameLine: true,
};