const path = require('path')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const { VueLoaderPlugin } = require('vue-loader')

module.exports = {
  mode: 'production',
  entry: path.resolve(__dirname, '../src/entry.js'),
  devtool: 'source-map',
  output: {
    library: 'vue-metha-ui',
    libraryTarget: 'umd',
    path: path.resolve(__dirname, '../dist'),
    publicPath: '/dist/'
  },
  resolve: {
    extensions: ['.vue', '.ts', '.js'],
    alias: {
      '@': path.resolve(__dirname, '..', 'src'),
    }
  },
  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        cache: true,
        parallel: true,
        sourceMap: true
        // compress: {
        //   pure_funcs: ['console.log']
        // }
      }),
      new OptimizeCSSAssetsPlugin({
        canPrint: true
      })
    ]
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.ts$/,
        loader: 'ts-loader',
        exclude: /node_modules/,
        options: {
          appendTsSuffixTo: [/\.vue$/]
        }
      },
      {
        test: /\.((c|sa|sc)ss)$/i,
        use: [
          'vue-style-loader',
          {
            loader: 'css-loader',
            options: {
              modules: {
                localIdentName: 'me-c-[hash:base64:8]'
              },
            }
          },
          {
            loader: 'sass-loader',
            options: {
              data: `
                @import "./src/assets/scss/variables/_colors.scss";
              `
            }
          }

        ]
      }
    ]
  },
  plugins: [new VueLoaderPlugin()]
}